#!/bin/bash

array=();
while IFS=  read -r line; do
    array+=("$line")
#done < <(find . -name "Dockerfile" -print0)
done < pathDockerfiles.txt
cd ..

echo ${array[@]};

for i in "${array[@]}"
do 
    echo "Check syntax for ${i}";
    hadolint ${i};
done

echo "==============================";
echo "Stage hadolint is done!";
echo "==============================";
